import Head from 'next/head';
import Image from 'next/image';
import styles from './layout.module.css';
import utilStyles from '../../styles/utils.module.css';
import Link from 'next/link';
import cloudinaryLoader from '../../libs/Loader'

const name = 'Kevin Alexis Rodriguez Condori';
export const siteTitle = 'Next.js Sample Website';

export default function Layout({ children, home }) {
    return (
        <div className={styles.container}>
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <meta
                    name="description"
                    content="Learn how to build a personal website using Next.js"
                />
                <meta
                    property="og:image"
                    content={`https://og-image.vercel.app/${encodeURI(
                        siteTitle,
                    )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
                />
                <meta name="og:title" content={siteTitle} />
                <meta name="twitter:card" content="summary_large_image" />
            </Head>
            <header className={styles.header}>
                {home ? (
                    <>
                        <Image
                            priority
                            className={utilStyles.borderCircle}
                            loader={cloudinaryLoader}
                            src="v1691598440/nft/Default_megapolis_cyberpunk_lighting_bar_single_girl_with_head_1_19f3a4d7-9993-44f6-b48e-71f67c289e02_1_lvtpfa.jpg"
                            height={300} // Desired size with correct aspect ratio
                            width={200} // Desired size with correct aspect ratio
                            alt=""
                        />
                        <h1 className={utilStyles.headingXl}>{name}</h1>
                    </>
                ) : (
                    <>
                        <Link href="/">
                            <Image
                                priority
                                loader={cloudinaryLoader}
                                src="v1691598440/nft/Default_megapolis_cyberpunk_lighting_bar_single_girl_with_head_1_19f3a4d7-9993-44f6-b48e-71f67c289e02_1_lvtpfa.jpg"
                                className={utilStyles.borderCircle}
                                height={108}
                                width={108}
                                alt=""
                            />
                        </Link>
                        <h2 className={utilStyles.headingLg}>
                            <Link href="/" className={utilStyles.colorInherit}>
                                {name}
                            </Link>
                        </h2>
                    </>
                )}
            </header>
            <main>{children}</main>
            {!home && (
                <div className={styles.backToHome}>
                    <Link href="/">← Back to home</Link>
                </div>
            )}
        </div>
    );
}