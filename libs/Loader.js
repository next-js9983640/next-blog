// Demo: https://res.cloudinary.com/demo/image/upload/w_300,c_limit,q_auto/turtles.jpg
export default function cloudinaryLoader({ src }) {
  
  return `https://res.cloudinary.com/dhq9acwqr/image/upload/${src}`;
}
